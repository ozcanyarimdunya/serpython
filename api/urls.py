from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import UserViewSet, PostViewSet, CommentViewSet

router = DefaultRouter()
router.register(r"user", UserViewSet, basename="user")
router.register(r"post", PostViewSet, basename="post")
router.register(r"comment", CommentViewSet, basename="comment")

app_name = "api"
urlpatterns = [
    path("", include(router.urls))
]
