import os
from datetime import datetime

from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase

User = get_user_model()


class ViewSetTestCase(APITestCase):
    def setUp(self) -> None:
        """
        Set SERIALIZER_TYPE to test serializer type
        If doesn't set then it will user api/serializers/regular.py
        :return:
        :rtype:
        """
        # os.environ.setdefault("SERIALIZER_TYPE", "serps")
        # os.environ.setdefault("SERIALIZER_TYPE", "model")
        os.environ.setdefault("SERIALIZER_TYPE", "overridden")

    def calculate(self, url, count):
        now = datetime.now()
        for i in range(1, count):
            response = self.client.get(f"{url}?page={i}")
            self.assertEqual(response.status_code, 200)
        print(url, datetime.now() - now)

    def test_user(self):
        """
        Serpy                : 0:00:00.097040
        ModelSerializer      : 0:00:00.243521
        RegularSerializer    : 0:00:00.136917
        OverriddenSerializer :
        """
        self.calculate("/api/user/", count=50)

    def test_posts(self):
        """
        Serpy                : 0:00:04.559367
        ModelSerializer      : 0:00:06.567857
        RegularSerializer    : 0:00:05.104409
        OverriddenSerializer :
        """
        self.calculate("/api/post/", count=50)

    def test_comments(self):
        """
        Serpy                : 0:00:12.197040
        ModelSerializer      : 0:00:14.572816
        RegularSerializer    : 0:00:13.427696
        OverriddenSerializer :
        """
        self.calculate("/api/comment/", count=50)
