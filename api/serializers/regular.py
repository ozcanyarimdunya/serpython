from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.Serializer):  # noqa
    id = serializers.IntegerField()
    username = serializers.CharField()
    email = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    class Meta:
        fields = (
            "id", "username", "email", "first_name", "last_name"
        )


class PostSerializer(serializers.Serializer):  # noqa
    id = serializers.IntegerField()
    author = UserSerializer()
    title = serializers.CharField()
    content = serializers.CharField()
    created = serializers.DateTimeField()
    updated = serializers.DateTimeField()

    class Meta:
        fields = (
            "id", "author", "title", "content", "created", "updated"
        )


class CommentSerializer(serializers.Serializer):  # noqa
    id = serializers.IntegerField()
    user = UserSerializer()
    post = PostSerializer()
    comment = serializers.CharField()
    created = serializers.DateTimeField()
    updated = serializers.DateTimeField()

    class Meta:
        fields = (
            "id", "user", "post", "comment", "created", "updated"
        )
