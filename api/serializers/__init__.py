import os

serializer = os.getenv("SERIALIZER_TYPE")

if serializer == "serps":
    from .serps import CommentSerializer, PostSerializer, UserSerializer
elif serializer == "model":
    from .model import CommentSerializer, PostSerializer, UserSerializer
elif serializer == "overridden":
    from .overridden import CommentSerializer, PostSerializer, UserSerializer
else:
    from .regular import CommentSerializer, PostSerializer, UserSerializer
