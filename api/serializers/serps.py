import serpy
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(serpy.Serializer):
    id = serpy.IntField()
    username = serpy.StrField()
    email = serpy.StrField()
    first_name = serpy.StrField()
    last_name = serpy.StrField()


class PostSerializer(serpy.Serializer):
    id = serpy.IntField()
    author = UserSerializer()
    title = serpy.StrField()
    content = serpy.StrField()
    created = serpy.StrField()
    updated = serpy.StrField()


class CommentSerializer(serpy.Serializer):
    id = serpy.IntField()
    user = UserSerializer()
    post = PostSerializer()
    comment = serpy.StrField()
    created = serpy.StrField()
    updated = serpy.StrField()
