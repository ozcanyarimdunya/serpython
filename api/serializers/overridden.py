from collections import OrderedDict

from django.contrib.auth import get_user_model
from django.utils.functional import cached_property
from rest_framework import serializers
from rest_framework.fields import empty, SkipField
from rest_framework.relations import PKOnlyObject
from rest_framework.utils.serializer_helpers import BindingDict

User = get_user_model()


class OverriddenSerializer(serializers.Serializer):
    def __init__(self, instance=None, data=empty, **kwargs):
        if kwargs.get("context"):
            request = kwargs["context"]["request"]
        super().__init__(instance=instance, data=data, **kwargs)

    @cached_property
    def fields(self):
        """
        A dictionary of {field_name: field_instance}.
        """
        # `fields` is evaluated lazily. We do this to ensure that we don't
        # have issues importing modules that use ModelSerializers as fields,
        # even if Django's app-loading stage has not yet run.
        fields = BindingDict(self)
        for key, value in self.get_fields().items():
            fields[key] = value
        return fields

    @property
    def _readable_fields(self):
        for field in self.fields.values():
            if not field.write_only:
                yield field

    def to_representation(self, instance):
        """
        Object instance -> Dict of primitive datatypes.
        """
        ret = OrderedDict()

        for field in self._readable_fields:
            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            # We skip `to_representation` for `None` values so that fields do
            # not have to explicitly deal with that case.
            #
            # For related fields with `use_pk_only_optimization` we need to
            # resolve the pk value.
            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class UserSerializer(OverriddenSerializer):
    id = serializers.IntegerField()
    username = serializers.CharField()
    email = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    class Meta:
        fields = (
            "id", "username", "email", "first_name", "last_name"
        )


class PostSerializer(OverriddenSerializer):
    id = serializers.IntegerField()
    author = UserSerializer()
    title = serializers.CharField()
    content = serializers.CharField()
    created = serializers.DateTimeField()
    updated = serializers.DateTimeField()

    class Meta:
        fields = (
            "id", "author", "title", "content", "created", "updated"
        )


class CommentSerializer(OverriddenSerializer):
    id = serializers.IntegerField()
    user = UserSerializer()
    post = PostSerializer()
    comment = serializers.CharField()
    created = serializers.DateTimeField()
    updated = serializers.DateTimeField()

    class Meta:
        fields = (
            "id", "user", "post", "comment", "created", "updated"
        )
