from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class AbstractTrackableModel(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Post(AbstractTrackableModel):
    class Meta:
        ordering = ["-created"]

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=240)
    content = models.TextField()

    def __str__(self):
        return self.title


class Comment(AbstractTrackableModel):
    class Meta:
        ordering = ["-created"]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.CharField(max_length=240)

    def __str__(self):
        return self.comment
