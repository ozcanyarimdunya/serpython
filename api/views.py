from django.contrib.auth import get_user_model
from rest_framework.viewsets import ModelViewSet

from .models import Post, Comment
from .serializers import UserSerializer, PostSerializer, CommentSerializer

User = get_user_model()


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CommentViewSet(ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
