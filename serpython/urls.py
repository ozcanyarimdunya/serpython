from django.conf import settings
from django.urls import path, include

urlpatterns = [
    path("api/", include("api.urls"))
]

if settings.DEBUG:
    from django.contrib import admin
    from django.conf.urls.static import static
    from rest_framework import permissions
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title="API Docs",
            default_version='v1',
            description="API Documentation",
            contact=openapi.Contact(email="ozcanyd@gmail.com"),
            license=openapi.License(name="MIT License"),
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),  # noqa
    )

    urlpatterns += [
        path('admin/', admin.site.urls),
        path('docs/', schema_view.with_ui('swagger', cache_timeout=0)),  # noqa
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
