# Serializer Optimizations

## Installation

**Install the requirements**

```shell
pip install -r requirements.txt
```

**Generate dummy data**

```shell
python generate_data.py
```

## Test

Check [tests.py](api/tests.py) `setUp` method and related name to use test serializers

1. To user serpy set `SERIALIZER_TYPE` to `serps` .
2. To user model serializer set `SERIALIZER_TYPE` to `model` .
3. To user overridden serializer set `SERIALIZER_TYPE` to `overridden` .
4. To user regular serializer set `SERIALIZER_TYPE` to `None` .

Run tests

```shell
python manage.py test
```

## Optimizations

1. Check previous serializers (`serps, model and regular`) first
2. Try to optimize OverriddenSerializer base class.

How to optimize ?

Serpy does these steps

1. Get defined fields in serializer
2. Get representation of each fields

How to get fields in drf?

1. Get defined fields in serializer

Django rest base `serializers.Serializer` class has a metaclass `SerializerMetaclass`. A metaclass runs before
constructor(__init__) when object created. In here it tries to get `_declared_fields`, the fields type of Field declared
in serializer class
`get_fields` in `serializers.Serializer` class copy this `declared_fields`

2. Get representation of each fields

`to_representation` get readable fields from `declared_fields` and tries to get representation of field.

**NOTES:**

`serializers.Serializer` inherits from `serializers.BaseSerializer`. If data is type of list it tries to re-generate a
new serializer with instance of `serializers.ListSerializer`.

I couldn't find a way to optimize the serializer :(
