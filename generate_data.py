import os
import random
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'serpython.settings')
django.setup()

from api.models import User, Post, Comment  # noqa

count = 500


def create_users():
    data = []
    for each in range(1, count + 1):
        data.append(
            User(
                username=f"test-{each}",
                email=f"test-{each}@test.com",
                first_name=f"First Test-{each}",
                last_name=f"Last Test-{each}"
            )
        )
    User.objects.bulk_create(data)
    print("Create total users: ", User.objects.count())


def create_posts():
    data = []
    for each in range(1, count + 1):
        authors = random.choices([i for i in range(1, count + 1)], k=100)
        for author_id in authors:
            data.append(
                Post(
                    author_id=author_id,
                    title=f"Post-{each}-{author_id}",
                    content=f"Content-{each}-{author_id}"
                )
            )
    Post.objects.bulk_create(data)
    print("Create total posts: ", Post.objects.count())


def create_comments():
    data = []
    for each in range(1, count + 1):
        _users = random.choices([i for i in range(1, count + 1)], k=random.randint(1, count))
        for user_id in _users:
            data.append(
                Comment(
                    user_id=user_id,
                    post_id=each,
                    comment=f"Comment to post: {each} by user: {user_id}"
                )
            )
    Comment.objects.bulk_create(data)
    print("Create total comments: ", Comment.objects.count())


if __name__ == '__main__':
    create_users()
    create_posts()
    create_comments()
